class Code
  attr_reader :pegs

  PEGS = {
    "R" => :red,
    "G" => :green,
    "B" => :blue,
    "O" => :orange,
    "P" => :purple,
    "Y" => :yellow
  }

  def initialize(pegs_arr)
    @pegs = pegs_arr
  end

  def self.random
    code_arr = []
    until code_arr.count == 4
      code_arr << PEGS.keys.sample
    end
    self.parse(code_arr.join)
  end

  def self.parse(input_str)
    str = input_str.upcase
    pegs = str.chars.map do |letter|
      raise "parse error" if PEGS[letter] == nil
      PEGS[letter]
    end
    Code.new(pegs)
  end

  def [](idx)
    pegs[idx]
  end

  def exact_matches(other_code)
    exact_count = 0
    (0..3).each {|i| exact_count += 1 if self.pegs[i] == other_code.pegs[i]}
    exact_count
  end

  def near_matches(other_code)
    near_count = 0
    colors = self.pegs
    other_code.pegs.each do |peg|
      if colors.include?(peg)
        near_count += 1
        colors.delete_at(colors.index(peg))
      end
    end
    near_count - exact_matches(other_code)
  end

  def ==(other_code)
    return false if !other_code.is_a?(Code)
    self.pegs == other_code.pegs
  end

end

class Game
  attr_reader :secret_code
  attr_accessor :turns

  def initialize(input_code=Code.random)
    @secret_code = input_code
    @turns = 0
  end

  def get_guess
    puts "Input a guess"
    input = gets.chomp
    Code.parse(input)
  end

  def display_matches(code)
    #looking for regular expression. No capitals.
    puts "exact matches: #{@secret_code.exact_matches(code)}"
    puts "near matches: #{@secret_code.near_matches(code)}"
  end

  def self.play(input_code=Code.random)
    mastermind = Game.new(input_code)
    guess = mastermind.get_guess
    until guess == mastermind.secret_code || mastermind.turns == 10
      mastermind.display_matches(guess)
      mastermind.turns += 1
      guess = mastermind.get_guess
    end
    return puts "Congratulations You Win! It took you #{@turns + 1} guesses." if guess == @secret_code
    puts "YOU LOSE"
  end
end

#can't get game to play properly
#Game.play
